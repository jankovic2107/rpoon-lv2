﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConsoleApp18
{
    class Program
    {
        static void Main(string[] args)
        {
            //Random randomGenerator = new Random(); 2. zadatak
            //FileLogger fileLogger = new FileLogger(@"C:\Users\Ivan Jankovic\Desktop\fileLogger.txt"); 4. zadatak
            //DiceRoller diceRoller = new DiceRoller(fileLogger); 4. zadatak
            FlexibleDiceRoller flexibleDiceRoller = new FlexibleDiceRoller();
            ConsoleLogger consoleLogger = new ConsoleLogger();
            Die die = new Die(6);
            DiceRoller diceRoller = new DiceRoller(consoleLogger);
            for (int i = 0; i < 20; i++)
            {
                flexibleDiceRoller.InsertDie(die);
            }
            flexibleDiceRoller.RollAllDice();
            //foreach(var result in diceRoller.GetRollingResults()) 4. zadatak
            //{
            //    //Console.WriteLine(result); 1.-3. zadatka
            //    consoleLogger.Log(result.ToString()); 4. zadatak
            //}
            consoleLogger.Log(flexibleDiceRoller);
        }
    }
}
