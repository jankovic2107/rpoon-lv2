﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp18
{
    class ConsoleLogger : ILogger
    {
        //public void Log(string message) 4. zadatak 
        //{
        //    Console.WriteLine(message);
        //}
        public void Log(ILoggable data)
        {
            Console.WriteLine(data.GetStringRepresentation());
        }
    }
}
