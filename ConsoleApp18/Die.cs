﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp18
{
    class Die
    {
        private int numberOfSides;
        private RandomGenerator randomGenerator;

        public Die(int numberOfSides) 
        {
            this.numberOfSides = numberOfSides;
            //this.randomGenerator = new Random(); 1.zadatak
            this.randomGenerator = RandomGenerator.GetInstance();
        }

        public int Roll()
        {
            int rolledNumber = randomGenerator.NextInt(1, numberOfSides + 1);
            return rolledNumber;
        }
    }
}
