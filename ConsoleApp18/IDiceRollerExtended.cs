﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp18
{
    interface IDiceRollerExtended
    {
        void InsertDie(Die die);
        void RemoveAllDice();
    }
}
