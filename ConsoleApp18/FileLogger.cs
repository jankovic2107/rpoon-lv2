﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp18
{
    class FileLogger : ILogger
    {
        private String filePath;

        public FileLogger(string filePath)
        {
            this.filePath = filePath;
        }

        public void Log(ILoggable data)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath))
            {
                writer.WriteLine(data.GetStringRepresentation());
            }
        }
    }
}
